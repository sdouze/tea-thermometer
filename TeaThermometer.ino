//Libraries 
#include <U8g2lib.h>
#include <Wire.h>
#include <OneWire.h>
#include <DallasTemperature.h>

//Set up temp sensor
#define ONE_WIRE_BUS 2
float temp;
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

//Set up I2C display using a constuctor from U8g2 library
U8G2_SSD1306_128X32_UNIVISION_F_SW_I2C u8g2(U8G2_R0, SCL, SDA, U8X8_PIN_NONE);   

#define grLed 3
#define redLed 4


void temp_check(float temp)
{
  if (temp >= 26 and temp <= 28){
    digitalWrite(grLed, HIGH);
    digitalWrite(redLed, LOW);
  }
  else{
    digitalWrite(redLed, HIGH);
    digitalWrite(grLed, LOW);
  }
}

void setup()
{
  delay(500);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  Serial.begin(9600); //Starting serial for testing
  Serial.println("Tea Thermometer");
  sensors.begin(); //Starts the library for the temperature sensor
  u8g2.begin(); //Starts the library for the display
}

void loop()
{
  sensors.requestTemperatures();
  temp = sensors.getTempCByIndex(0);
  Serial.println(temp);
  u8g2.clearBuffer();
  u8g2.setFont(u8g2_font_ncenB14_tr);
  u8g2.drawStr(0,15, "Temp is :");
  u8g2.setCursor(0,32);
  u8g2.print(temp);
  u8g2.sendBuffer();
  delay(200);
  temp_check(temp);

}
